import argparse
import time
import os
from datetime import date
from pathlib import Path

import pandas as pd
import pywikibot
import requests
from requests.exceptions import HTTPError

HEADERS = {"User-Agent": "Add-Letterboxd-ID"}
CUR_DIR = Path(__file__).parent.absolute()


def keyIndicators(test=False):
    def getKeyIndicators(indicatorsDict):
        for indicator in indicatorsDict:
            with (CUR_DIR / (indicator + ".sparql")).open() as r:
                query = r.readlines()
                query = "".join(query)

            indicatorsDict[indicator]["count"] = getData(query)

        return indicatorsDict

    indicatorsDict = {
        "num-films": {
            "title": "number of films",
        },
        "num-lost": {
            "title": "number of lost films",
        },
        "num-core": {
            "title": "number of films with [[Wikidata:WikiProject_Movies/Properties#Core_properties|core properties]]",
        },
        "num-core-5": {
            "title": "number of films with [[Wikidata:WikiProject_Movies/Properties#Core_properties|core properties]] and cast of 5 or more",
        },
        "num-directors": {
            "title": "number of directors",
        },
        "num-actors": {
            "title": "number of actors",
        },
    }

    indicators = getKeyIndicators(indicatorsDict)
    output = ""

    for indicator in indicators:
        output += (
            f";{indicators[indicator]['title']}: {indicators[indicator]['count']:,}\n"
        )

    output += "\nUpdated " + date.today().strftime("%d %B %Y") + "\n"

    if not test:
        makeEdit("Wikidata:WikiProject Movies/Numbers/Key Indicators", output)
    else:
        print(output)


def properties(test=False):
    def getCounts(props):
        counts = {}
        with (CUR_DIR / "property.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        for prop in props:
            counts[prop] = getData(query, prop)

        return counts

    def makeDataframe(counts):
        df = pd.DataFrame.from_dict(
            data=counts,
            orient="index",
            columns=["count"],
        )

        df["percentage"] = df["count"] / df["count"]["P31"]
        # df["top items to edit"]
        return df

    def makeOutput(df):
        today = date.today().strftime("%d %B %Y")
        output = (
            """<noinclude>__NOINDEX__</noinclude>Number of film items with a given property. Without external identifiers and other string properties. Updated: """
            + today
            + """\n<table class='wikitable sortable' style="font-size:smaller"><tr><th>property<th>count<th>percentage</tr>\n"""
        )
        for p, cols in df.iterrows():
            output += (
                "<tr><td>{{P|"
                + p
                + "}}<td>"
                + "{:,.0f}".format(cols["count"])
                + "<td>"
                + "{:.2f}".format(cols["percentage"] * 100)
                + "</tr>\n"
            )

        output += "</table>"

        return output

    core = ["P31", "P1476", "P577", "P364", "P136", "P57", "P161", "P344", "P58"]
    main = ["P1040", "P162", "P840", "P915", "P495"]
    other = ["P18", "P3383", "P10", "P1431", "P272", "P144", "P921", "P462", "P2047"]
    external = ["P345", "P4947"]
    data = [*core, *main, *other, *external]

    counts = getCounts(data)
    df = makeDataframe(counts)
    output = makeOutput(df)

    if not test:
        makeEdit("Wikidata:WikiProject Movies/Numbers/Properties", output)
    else:
        print(output)


def externalIdentifiers(test=False):
    def getExternalIdentifiers():
        output = {}
        with (CUR_DIR / "external-id.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = runQuery(query)

        for o in data["results"]["bindings"]:
            output[o["item"]['value'][31:]] = {'total': int(o['numrecords']['value'])}

        return output

    def getCoverage(ids):
        with (CUR_DIR / "coverage.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        for prop in ids.keys():
            ids[prop]["coverage"] = getData(query, prop)

        df = pd.DataFrame.from_dict(
            data=ids,
            orient="index",
            columns=['coverage', "total"],
        )

        df["percentage"] = df["coverage"] / df["total"]
        return df

    def makeOutput(df):
        today = date.today().strftime("%d %B %Y")
        output = (
            """<noinclude>__NOINDEX__\n</noinclude>Number of film items with a given external identifier. Updated: """
            + today
            + """\n<table class='wikitable sortable'><tr><th>property<th>items on Wikidata<th>total in source<th>coverage</tr>\n"""
        )
        for p, cols in df.iterrows():
            output += (
                "<tr><td>{{P|"
                + p
                + "}}<td>"
                + "{:,.0f}".format(cols["coverage"])
                + "<td>"
                + "{:,.0f}".format(cols["total"])
                + "<td>"
                + "{:.2f}".format(cols["percentage"] * 100)
                + "</tr>\n"
            )

        output += "</table>"

        return output

    ids = getExternalIdentifiers()
    # print(len(ids))
    coverage = getCoverage(ids)
    # print(len(coverage))
    output = makeOutput(coverage)

    if not test:
        makeEdit("Wikidata:WikiProject_Movies/Numbers/External_Identifiers", output)
    else:
        print(output)


def getData(query, prop=""):
    if prop:
        data = runQuery(
            query.format(
                prop=prop,
            )
        )
    else:
        data = runQuery(query)

    return int(data["results"]["bindings"][0]["count"]["value"])


def runQuery(query):
    url = "https://query.wikidata.org/sparql"
    params = {"query": query, "format": "json"}
    try:
        response = requests.get(url, params=params, headers=HEADERS)
        return response.json()
    except HTTPError as e:
        print(response.text)
        print(e.response.text)
        print(query)
        return {"results": {"bindings": []}}
    except BaseException as err:
        print(query)
        print(f"Unexpected {err}, {type(err)}")
        raise


def makeEdit(page, string):
    site = pywikibot.Site("wikidata", "wikidata")
    site.login()
    page = pywikibot.Page(site, page)

    page.text = string
    page.save("Update statistics")


def timer(tick, msg=""):
    print("--- %s %.3f seconds ---" % (msg, time.time() - tick))
    return time.time()


def defineArgParser():
    """Creates parser for command line arguments"""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument(
        "-t",
        "--test",
        action="store_true",
    )

    return parser


if __name__ == "__main__":

    argParser = defineArgParser()
    clArgs = argParser.parse_args()

    tick = time.time()
    keyIndicators(test=clArgs.test)
    properties(test=clArgs.test)
    externalIdentifiers(test=clArgs.test)
    timer(tick)
